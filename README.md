Charter Basic Component Library Test Project
I've had the pleasure to build this simple component library for the stellar team at Charter to show I have the basics of what it might take to be a part of the team.

Motivation
My motivation is to have a chance to work with the awesome team I've had a chance to meet so far at Charter. In order to do so, I am demonstrating here, that I can adequately build a vanilla component library, consisting of one or two simple components (without use of frameworks) and successfully package the library for public distribution, in order to function within a bootstrapped "Hello World" application.  

p.birlem@gmail.com 
