export const white = { hex: '#ffffff', rgb: '255,255,255' };
export const blue0 = { hex: '#b9dcf7', rgb: '185,220,247' };
export const blue1 = { hex: '#74b9ef', rgb: '116,185,239' };
export const blue2 = { hex: '#51a7eb', rgb: '81,167,235' };
export const blue3 = { hex: '#177AC9', rgb: '23,122,201' };
export const blue4 = { hex: '#285172', rgb: '40,81,114' };
export const blue5 = { hex: '#142939', rgb: '20,41,57' };
export const blue6 = { hex: '#899fb2', rgb: '137,159,178' };
export const grey = { hex: '#363636', rgb: '54,54,54' };
export const grey1 = { hex: '#e7ecf0', rgb: '231,236,240' };
export const grey2 = { hex: '#a1a1a1', rgb: '161,161,161' };
export const grey3 = { hex: '#363636', rgb: '54,54,54' };
export const grey4 = { hex: '#131313', rgb: '19,19,19' };
export const grey5 = { hex: '#9a9a9a', rgb: '154,154,154' };
export const grey6 = { hex: '#595959', rgb: '89,89,89' };
export const grey7 = { hex: '#707070', rgb: '112,112,112' };
export const grey8 = { hex: '#c4c4c4', rgb: '196,196,196' };
export const black = { hex: '#000000', rgb: '0,0,0' };
export const red1 = { hex: '#ff1e00', rgb: '255,30,0' };
export const red2 = { hex: '#cc292d', rgb: '204,41,45' };
export const green = { hex: '#2f8f28', rgb: '47,143,40' };
