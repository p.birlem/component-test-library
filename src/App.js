import React from 'react';
import HelloWorld from './component_library/HelloWorld.js';
import './App.css';

function App() {
  return (
    <div className="App">
      <HelloWorld />
    </div>
  );
}

export default App;
